package Domain;

public class Permissions {

    private int user;

    private int group;

    private int other;

    public Permissions(int user, int group, int other) {
        this.validate(user, group, other);
        this.user = user;
        this.group = group;
        this.other = other;
    }

    private void validate(int user, int group, int other) {
        if (user < 0 || user > 7)
            throw new IllegalArgumentException("User permissions not allowed.");
        if (group < 0 || group > 7)
            throw new IllegalArgumentException("Group permissions not allowed.");
        if (other < 0 || other > 7)
            throw new IllegalArgumentException("Other permissions not allowed.");
    }

    public int getUser() {
        return user;
    }

    public int getGroup() {
        return group;
    }

    public int getOther() {
        return other;
    }
}
