package Domain;

public enum FileType {
    txt,
    jpg,
    png,
    java,
    js,
    cs
}
