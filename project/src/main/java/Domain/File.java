package Domain;

public class File extends Entity {

    private FileType fileType;

    public File(Name name, ModificationDate modificationDate, Permissions permissions, Size size, FileType fileType) {
        super(name, modificationDate, permissions, size);
        this.fileType = fileType;
    }

    public FileType getFileType() {
        return fileType;
    }
}
