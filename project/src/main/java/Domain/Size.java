package Domain;

public class Size {

    private int size;

    public Size(int size) {
        this.size = size;
    }

    public int getSize() {
        return size;
    }
}
