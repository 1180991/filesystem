package Domain;

public abstract class Entity {

    private Name name;

    private ModificationDate modificationDate;

    private Permissions permissions;

    private Size size;

    public Entity(Name name, ModificationDate modificationDate, Permissions permissions, Size size) {
        this.name = name;
        this.modificationDate = modificationDate;
        this.permissions = permissions;
        this.size = size;
    }

    public Name getName() {
        return name;
    }

    public ModificationDate getModificationDate() {
        return modificationDate;
    }

    public Permissions getPermissions() {
        return permissions;
    }

    public Size getSize() {
        return size;
    }
}
