package Domain;

import java.util.Set;
import java.util.HashSet;

public class Directory extends Entity {

    private Set<Entity> entityList;

    public Directory(Name name, ModificationDate modificationDate, Permissions permissions, Size size) {
        super(name, modificationDate, permissions, size);
    }

    public Set<Entity> getEntityList() {
        return entityList;
    }

    public boolean addEntity(Entity newEntity) {
        return this.entityList.add(newEntity);
    }

    public boolean removeNotEmptyEntry(Entity toDeleteEntity) {
        if (toDeleteEntity instanceof Directory && ((Directory) toDeleteEntity).isEmpty())
            return this.removeEntity(toDeleteEntity);
        return false;
    }

    public boolean removeEntity(Entity toDeleteEntity) {
        return this.entityList.remove(toDeleteEntity);
    }

    public boolean isEmpty() {
        return entityList.isEmpty();
    }
}
