package Domain;

import java.util.Date;

public class ModificationDate {
    private Date date;

    public ModificationDate(Date date) {
        this.date = date;
    }

    public Date getDate() {
        return date;
    }
}
